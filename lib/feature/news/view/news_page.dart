import 'package:example_test/feature/news/view/article_page.dart';
import 'package:example_test/feature/news/notifier/news_change_notifier.dart';
import 'package:example_test/navigation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class NewsPage extends StatefulWidget {
  const NewsPage({super.key});

  @override
  State<NewsPage> createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  @override
  void initState() {
    super.initState();
    Future.microtask(
      () => context.read<NewsChangeNotifier>().getArticles(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('News'),
      ),
      body: Consumer<NewsChangeNotifier>(builder: (context, notifier, child) {
        if (notifier.isLoading) {
          return const Center(
              child: CircularProgressIndicator(
            key: Key('progress-indicator'),
          ));
        } else if (notifier.articles.isEmpty) {
          return const Center(
            child: Text('Empty'),
          );
        }

        return ListView.builder(
          itemCount: notifier.articles.length,
          itemBuilder: (context, index) {
            final article = notifier.articles[index];
            return InkWell(
              onTap: () {
                Navigation.push(
                  ArticlePage(
                    article: article,
                  ),
                );
              },
              child: Card(
                elevation: 2,
                child: ListTile(
                  title: Text(
                    article.title,
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  subtitle: Text(
                    article.content,
                    maxLines: 4,
                    style: const TextStyle(color: Colors.black),
                  ),
                ),
              ),
            );
          },
        );
      }),
    );
  }
}
