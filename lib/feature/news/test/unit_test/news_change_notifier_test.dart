import 'package:example_test/feature/news/model/article.dart';
import 'package:example_test/feature/news/notifier/news_change_notifier.dart';
import 'package:example_test/feature/news/service/news_service.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class MockNewsServices extends Mock implements NewsService {}

void main() {
  late NewsChangeNotifier newsChangeNotifier;
  late MockNewsServices mockNewsServices;

  setUp(() {
    mockNewsServices = MockNewsServices();
    newsChangeNotifier = NewsChangeNotifier(mockNewsServices);
  });

  test('initial value are corect', () {
    expect(newsChangeNotifier.articles, []);
    expect(newsChangeNotifier.isLoading, false);
  });

  group('get articles', () {
    final articles = [
      Article(title: 'Title 1', content: 'Content 1'),
      Article(title: 'Title 2', content: 'Content 2'),
      Article(title: 'Title 3', content: 'Content 3'),
    ];

    void newsServiceReturn3Articles() {
      when(
        () => mockNewsServices.getArticles(),
      ).thenAnswer((_) async => articles);
    }

    test('get articles using NewsServices', () async {
      newsServiceReturn3Articles();
      await newsChangeNotifier.getArticles();
      verify(
        () => mockNewsServices.getArticles(),
      ).called(1);
    });

    test(
      """indicates loading of data,
      sets articles to the ones from the service,
      indicates that data is not being loaded anymore""",
      () async {
        newsServiceReturn3Articles();
        final future = newsChangeNotifier.getArticles();
        expect(newsChangeNotifier.isLoading, true);
        await future;
        expect(newsChangeNotifier.articles, articles);
        expect(newsChangeNotifier.isLoading, false);
      },
    );
  });
}
