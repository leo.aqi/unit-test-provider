import 'package:example_test/feature/news/model/article.dart';
import 'package:example_test/feature/news/notifier/news_change_notifier.dart';
import 'package:example_test/feature/news/service/news_service.dart';
import 'package:example_test/feature/news/view/news_page.dart';
import 'package:example_test/main.dart';
import 'package:example_test/navigation.dart';
import 'package:flutter/material.dart';
import 'package:mocktail/mocktail.dart';
import 'package:provider/provider.dart';
import 'package:flutter_test/flutter_test.dart';

class MockNewsServices extends Mock implements NewsService {}

void main() {
  late MockNewsServices mockNewsServices;

  setUp(() {
    mockNewsServices = MockNewsServices();
  });

  final articlesFromService = [
    Article(title: 'Title 1', content: 'Content 1'),
    Article(title: 'Title 2', content: 'Content 2'),
    Article(title: 'Title 3', content: 'Content 3'),
  ];

  void newsServiceReturn3Articles() {
    when(() => mockNewsServices.getArticles())
        .thenAnswer((_) async => articlesFromService);
  }

  Widget createWidgetUnderTest() {
    return MaterialApp(
      title: 'News App',
      navigatorKey: Navigation.navigatorKey,
      home: ChangeNotifierProvider(
        create: (_) => NewsChangeNotifier(mockNewsServices),
        child: const NewsPage(),
      ),
    );
  }

  testWidgets(
    "Taping on the first article",
    (WidgetTester tester) async {
      newsServiceReturn3Articles();

      await tester.pumpWidget(createWidgetUnderTest());

      await tester.pump(const Duration(seconds: 2));

      await tester.tap(find.text('Title 1'));

      await tester.pumpAndSettle();

      expect(find.text('Detail article'), findsOneWidget);
      expect(find.text('Title 1'), findsOneWidget);
      expect(find.text('Content 1'), findsOneWidget);
    },
  );
}
