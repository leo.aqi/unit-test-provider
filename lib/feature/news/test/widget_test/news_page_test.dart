import 'package:example_test/feature/news/model/article.dart';
import 'package:example_test/feature/news/notifier/news_change_notifier.dart';
import 'package:example_test/feature/news/service/news_service.dart';
import 'package:example_test/feature/news/view/news_page.dart';
import 'package:mocktail/mocktail.dart';
import 'package:provider/provider.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';

class MockService extends Mock implements NewsService {}

void main() {
  late MockService mockService;

  setUp(() {
    mockService = MockService();
  });

  final articlesFromService = [
    Article(title: 'Title 1', content: 'Content 1'),
    Article(title: 'Title 2', content: 'Content 2'),
    Article(title: 'Title 3', content: 'Content 3'),
  ];

  void newsServiceReturn3Articles() {
    when(
      () => mockService.getArticles(),
    ).thenAnswer((_) async => articlesFromService);
  }

  void newsServiceReturn3ArticlesWithDelay() {
    when(
      () => mockService.getArticles(),
    ).thenAnswer((_) async {
      await Future.delayed(const Duration(seconds: 2));
      return articlesFromService;
    });
  }

  Widget createWidgetUnderTest() {
    return MaterialApp(
      title: 'News App',
      home: ChangeNotifierProvider(
        create: (_) => NewsChangeNotifier(mockService),
        child: const NewsPage(),
      ),
    );
  }

  testWidgets(
    "title is displayed",
    (WidgetTester tester) async {
      newsServiceReturn3Articles();

      await tester.pumpWidget(createWidgetUnderTest());

      expect(find.text('News'), findsOneWidget);
    },
  );

  testWidgets(
    "loading indicator is delayed while waiting for articles",
    (WidgetTester tester) async {
      newsServiceReturn3ArticlesWithDelay();

      await tester.pumpWidget(createWidgetUnderTest());
      await tester.pump(const Duration(microseconds: 100));

      expect(find.byKey(const Key('progress-indicator')), findsOneWidget);
      await tester.pumpAndSettle();
    },
  );

  testWidgets(
    "article are displayed",
    (WidgetTester tester) async {
      newsServiceReturn3Articles();

      await tester.pumpWidget(createWidgetUnderTest());

      await tester.pump();

      for (final article in articlesFromService) {
        expect(find.text(article.title), findsOneWidget);
        expect(find.text(article.content), findsOneWidget);
      }
    },
  );
}
