import 'package:flutter/material.dart';

class Navigation {
  static final GlobalKey<NavigatorState> navigatorKey =
      GlobalKey<NavigatorState>();

  static push(Widget child) {
    Navigator.push(
        navigatorKey.currentContext!, MaterialPageRoute(builder: (_) => child));
  }

  static back() {
    Navigator.pop(navigatorKey.currentContext!);
  }
}
